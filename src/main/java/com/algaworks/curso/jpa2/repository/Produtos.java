package com.algaworks.curso.jpa2.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.algaworks.curso.jpa2.model.Produto;

public class Produtos implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	
	public Produto guardar(Produto produto) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		produto = manager.merge(produto);
		
		trx.commit();
		
		return produto;
	}

		
	@SuppressWarnings("unchecked")
	public List<Produto> buscarTodos() {
		
		return manager.createQuery("from Produto").getResultList();
	}


	public Produto porId(Long id) {
		return manager.find(Produto.class, id);
	}
	
}
