package com.algaworks.curso.jpa2.Dao;

import java.io.Serializable;
import java.nio.charset.CodingErrorAction;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.model.Fabricante;
import com.algaworks.curso.jpa2.service.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class FabricanteDAO implements Serializable{

	@Inject
	private EntityManager em;
	
	
	public void salvar(Fabricante fabricante){
		
		em.merge(fabricante);
	}


	@SuppressWarnings("unchecked")
	public List<Fabricante> buscarTodos() {
		
		return em.createQuery("from Fabricante").getResultList();
	}

    @Transactional
	public void excluir(Fabricante fabricante) throws NegocioException{
		
		fabricante = em.find(Fabricante.class, fabricante.getCodigo());
		em.remove(fabricante);
		//flush = execulte o sql agora
		em.flush();
	}


	public Fabricante buscarPeloCodigo(Long codigo) {
		
		return em.find(Fabricante.class, codigo);
	}

}
