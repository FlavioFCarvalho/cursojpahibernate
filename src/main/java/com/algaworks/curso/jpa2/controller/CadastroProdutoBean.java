package com.algaworks.curso.jpa2.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.curso.jpa2.model.Produto;
import com.algaworks.curso.jpa2.service.CadastroProdutoService;
import com.algaworks.curso.jpa2.service.NegocioException;
import com.algaworks.curso.jpa2.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroProdutoBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroProdutoService cadastroProdutoService;
	
	private Produto produto;
	
	
	
	public CadastroProdutoBean() {
		limpar();
	}
	
	private void limpar() {
		produto = new Produto();
	}
	
	/*public void salvar() throws NegocioException {
		this.produto = cadastroProdutoService.salvar(this.produto);
		limpar();
		
		FacesUtil.addInfoMessage("Produto salvo com sucesso!");
	}*/
	
	
	public void salvar() throws NegocioException{
		this.produto = cadastroProdutoService.salvar(this.produto);
		limpar();
		FacesUtil.addSuccessMessage("Produto salvo com sucesso!");
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
		
	
}
